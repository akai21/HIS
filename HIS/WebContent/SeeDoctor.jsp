<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5shiv.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="static/h-ui.admin/css/style.css" />
<!--[if IE 6]>
<script type="text/javascript" src="lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<!--/meta 作为公共模版分离出去-->

<title>医生</title>
</head>
<script type="text/javascript">
function getDrugs() {
	var drugName=document.getElementById("drugName").value;
	$("#tbMain  tr:not(:first)").empty("");
	$.getJSON(
			"GetDrugsByNameServlet",
			{"drugName":drugName},
			function(result){
				var tbody = document.getElementById('tbMain');
				var drugs = eval(result);
				$.each(drugs,
						function(i,element)
						{
					  		var trow = getDataRow(this); //定义一个方法,返回tr数据
				  	  		trow.setAttribute("class","text-c");
					  		tbody.appendChild(trow);
						});
			}
	);
}

function getDataRow(presDrug){
	var row = document.createElement('tr'); //创建行
	
	
	var idCell = document.createElement('td'); //name
	idCell.innerHTML = presDrug.id; //填充数据
	row.appendChild(idCell); //加入行  ，下面类似
	
	var nameCell = document.createElement('td'); //name
	nameCell.innerHTML = presDrug.name; //填充数据
	row.appendChild(nameCell); //加入行  ，下面类似
	
	var addCell = document.createElement('td');
	var add = document.createElement('input');
	add.setAttribute("class" ,"btn btn-primary radius");
	add.setAttribute("type" ,"button");
	add.setAttribute("onclick" ,"addDrug(this)");
	add.setAttribute("value" ,"添加");
	addCell.appendChild(add);
	row.append(addCell);
	
	return row;
}

function addDrug(addCell){
	var tbMain0 = document.getElementById("tbMain0");
	var newRow = addCell.parentNode.parentNode;
	
	var row = document.createElement('tr'); //创建行
	var idCell = document.createElement('td'); //name
	idCell.innerHTML = newRow.getElementsByTagName("td")[0].innerHTML; //填充数据
	row.appendChild(idCell); //加入行  ，下面类似
	
	var nameCell = document.createElement('td'); //name
	nameCell.innerHTML = newRow.getElementsByTagName("td")[1].innerHTML; ; //填充数据
	row.appendChild(nameCell); //加入行  ，下面类似

	
	var addCell = document.createElement('td');
	var add = document.createElement('input');
	add.setAttribute("class" ,"btn btn-primary radius");
	add.setAttribute("type" ,"button");
	add.setAttribute("onclick" ,"deleteDrug(this)");
	add.setAttribute("value" ,"删除");
	addCell.appendChild(add);
	row.append(addCell);
	
	var frequencyCell = document.createElement('td');
	var frenquency = document.createElement('input');
	frenquency.setAttribute("class" ,"input-text");
	frenquency.setAttribute("type" ,"text");
	frequencyCell.appendChild(frenquency);
	row.append(frequencyCell);
	
	
	var usageCell = document.createElement('td');
	var usage = document.createElement('input');
	usage.setAttribute("class" ,"input-text");
	usage.setAttribute("type" ,"text");
	usageCell.appendChild(usage);
	row.append(usageCell);	
	
	var numbersCell = document.createElement('td');
	var numbers = document.createElement('input');
	numbers.setAttribute("class" ,"input-text");
	numbers.setAttribute("type" ,"text");
	numbersCell.appendChild(numbers);
	row.append(numbersCell);
	
	tbMain0.appendChild(row);

	
}

function deleteDrug(cell){
	cell.parentNode.parentNode.parentNode.removeChild(cell.parentNode.parentNode);
}

function drugsSubmit(){
	var registId = document.getElementById("registId").value;
	var prescriptionName = document.getElementById("prescriptionName").value;
	var tab = document.getElementById('tbMain0');
	var data = "registId=" + registId + "&prescriptionName=" + prescriptionName + "&counts=" + String(tab.rows.length-1);
    for(var i=1; i<tab.rows.length; i++) {
    	data = data + "&drugId"+i +"="+tab.rows[i].cells[0].innerHTML + "&frequency"+i +"="+tab.rows[i].cells[3].firstChild.value + "&usage"+i +"="+tab.rows[i].cells[4].firstChild.value + "&numbers"+i +"="+tab.rows[i].cells[5].firstChild.value;
    }
	$.ajax({
		url:"PrescriptionServlet",
		请求方式:"post",
		data:data,
		success:function(result,testStatus){
			if(result=="true")
				alert("success");
			else
				alert("false");
		},
		error :function(){
			
		}
		
	});
}


</script>
<body>
<div class="page-container">
		<div id="tab-system" class="HuiTab">
			<div class="tabBar cl">
				<span>病历首页</span>
				<span>检查申请</span>
				<span>检验申请</span>
				<span>门诊确诊</span>
				<span>处置申请</span>
				<span>成药处方</span>
				<span>草药处方</span>
				<span>费用查询</span>
			</div>
			<div class="tabCon">
				<form class="form form-horizontal" id="form-article-add" action="SeeDoctorServlet" method="post">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">主诉：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<textarea class="textarea" placeholder="发烧，咳嗽" rows="1"  id="chiefcomplaint" name="chiefcomplaint"></textarea>
					</div>
				</div>
				
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">现病史：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<textarea class="textarea" placeholder="无" rows="1"  name="currentMedicalHistory"></textarea>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">现病治疗情况：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<textarea class="textarea" placeholder="无" rows="1" name="treatmentSituation"></textarea>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">既往病史：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<textarea class="textarea" placeholder="无" rows="1" name="previous"></textarea>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">过敏史：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<textarea class="textarea" placeholder="无" rows="1" name="Allergies"></textarea>
					</div>
				</div>
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">体格检查：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<textarea class="textarea" placeholder="无明显异常" rows="1" name="examination"></textarea>
					</div>
				</div>
				
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">建议：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<textarea class="textarea" placeholder="无" rows="1" id="suggestion" name="suggestion"></textarea>
					</div>
				</div>
				
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">注意事项：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<textarea class="textarea" placeholder="无" rows="1" id="precautions" name="precautions"></textarea>
					</div>
				</div>
				
							<div class="row cl">	
					<div class="col-xs-1">疾病：</div>
				<label class="form-label col-xs-1 col-sm-1">名称：</label>
			<div class="formControls col-xs-2 col-sm-2">
				<span class="select-box" >
				<select class="select" id="disease" name="disease" onclick="getDisease()">
				</select>
				</span>
			</div>
			
			
	
				<label class="form-label col-xs-1 col-sm-1">种类：</label>
			<div class="formControls col-xs-2 col-sm-2">
				<span class="select-box">
				<select class="select" id="kind" name="kind">
					<option value=1>初诊</option>
					<option value=2>终诊</option>
				</select>
				</span>
			</div>
			
				<label class="form-label col-xs-1 col-sm-1">类型：</label>
			<div class="formControls col-xs-2 col-sm-2">
				<span class="select-box" >
				<select class="select" id="type" name="type">
					<option value=1>西医</option>
					<option value=2>中医</option>
				</select>
				</span>
			</div>
			
				
									
			</div>
			
		<div class="row cl">
			<div class="col-xs-8 col-sm-9 col-xs-offset-4 col-sm-offset-2">
				<button  class="btn btn-primary radius" type="submit"><i class="Hui-iconfont">&#xe632;</i> 保存</button>
				<button onClick="layer_close();" class="btn btn-default radius" type="button">&nbsp;&nbsp;取消&nbsp;&nbsp;</button>
			</div>
		</div>
		</form>
		
		</div>
	
				
			<div class="tabCon">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">检查申请：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<textarea class="textarea" name="" id=""></textarea>
					</div>
				</div>				
			</div>
			<div class="tabCon">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">检验申请：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text"  class="input-text" value="" id="" name="">
					</div>
				</div>				
			</div>
			<div class="tabCon">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">门诊确诊：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text"  class="input-text" value="" id="" name="">
					</div>
				</div>				
			</div>
			<div class="tabCon">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">处置申请：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text"  class="input-text" value="" id="" name="">
					</div>
				</div>				
			</div>
			
			<div class="tabCon">
				<div class="row cl">
			<label class="form-label col-xs-1 col-sm-1"><span class="c-red">*</span>挂号ID：</label>
			<div class="formControls col-xs-1 col-sm-1">
			<input type="text" class="input-text" value="" placeholder="" id="registId" name="registId" >
			</div>
			<div class="formControls col-xs-1 col-sm-1">
			<input class="btn btn-primary radius" type="button"  onclick="drugsSubmit()" value="提交">
			</div>
			<label class="form-label col-xs-1 col-sm-1"><span class="c-red">*</span>处方名称：</label>
			
			<div class="formControls col-xs-4 col-sm-4">
				<input type="text" class="input-text" value="" placeholder="" id="prescriptionName" name="prescriptionName" >
				<table
				class="table table-border table-bordered table-bg table-hover table-sort table-responsive" id="tbMain0">
				<thead>
					<tr class="text-c">
						<th width="50" >药品id</th>
						<th width="100">药品名称</th>
						<th width="100">操作</th>
						<th width="100">频次</th>
						<th width="100">用法</th>
						<th width="100">数量</th>
					</tr>
				</thead>
				</table>
			</div>
		
			
			<label class="form-label col-xs-1 col-sm-1"><span class="c-red">*</span>药品信息：</label>
			<div class="formControls col-xs-2 col-sm-2">
				<input type="text" class="input-text" value="" placeholder="" id="drugName" name="drugName" >
				<div class="mt-20">
			<table
				class="table table-border table-bordered table-bg table-hover table-sort table-responsive" id="tbMain">
				<thead>
					<tr class="text-c">
						<th width="200" >药品id</th>
						<th width="200">药品名称</th>
						<th width="100">操作</th>
					</tr>
				</thead>
				</table>
				</div>
			</div>
			
			<div class="formControls col-xs-1 col-sm-1">
				<input class="btn btn-primary radius" type="button"  onclick="getDrugs()" value="查询">
			</div>
			</div>
			</div>
			<div class="tabCon">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">草药处方：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text"  class="input-text" value="" id="" name="">
					</div>
				</div>				
			</div>
			<div class="tabCon">
				<div class="row cl">
					<label class="form-label col-xs-4 col-sm-2">费用查询：</label>
					<div class="formControls col-xs-8 col-sm-9">
						<input type="text"  class="input-text" value="" id="" name="">
					</div>
				</div>				
			</div>
	
	</div>	
	</div>

<!--_footer 作为公共模版分离出去-->
<script type="text/javascript" src="lib/jquery/1.9.1/jquery.min.js"></script>
<script type="text/javascript" src="lib/layer/2.4/layer.js"></script>
<script type="text/javascript" src="static/h-ui/js/H-ui.min.js"></script>
<script type="text/javascript" src="static/h-ui.admin/js/H-ui.admin.js"></script> <!--/_footer 作为公共模版分离出去-->

<!--请在下方写此页面业务相关的脚本-->
<script type="text/javascript" src="lib/My97DatePicker/4.8/WdatePicker.js"></script>
<script type="text/javascript" src="lib/jquery.validation/1.14.0/jquery.validate.js"></script>
<script type="text/javascript" src="lib/jquery.validation/1.14.0/validate-methods.js"></script>
<script type="text/javascript" src="lib/jquery.validation/1.14.0/messages_zh.js"></script>
<script type="text/javascript">
$(function(){
	$('.skin-minimal input').iCheck({
		checkboxClass: 'icheckbox-blue',
		radioClass: 'iradio-blue',
		increaseArea: '20%'
	});
	$("#tab-system").Huitab({
		index:0
	});
});
function getDisease(){
	 var selections = document.getElementById("disease");
	 selections.innerHTML = "";
	 for(var i=0;i<selections.childNodes.length;i++){
			selections.removeChild(area.options[0]);
			selections.remove(0);
			selections.options[0] = null;
	}
	$.getJSON(
			"GetDiseaseServlet",
			function(result){
				var diseases = eval(result);
				$.each(diseases,
						function(i,element)
						{
							//设置下拉列表中的值的属性
		                   var option = document.createElement("option");
		                   option.value = this.name;
		                   option.text= this.name;
		                   //将option增加到下拉列表中。
		                    selections.add(option);
						});
			}
	);
}
</script>
<!--/请在上方写此页面业务相关的脚本-->
</body>
</html>
