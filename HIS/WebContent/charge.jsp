<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!--_meta 作为公共模版分离出去-->
<!DOCTYPE HTML>
<html>
 <style>
  #popLayer {
            display: none;
            background-color: #B3B3B3;
            position: absolute;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            z-index: 10;
            -moz-opacity: 0.8;
            opacity:.80;
            filter: alpha(opacity=80);/* 只支持IE6、7、8、9 */
        }
        #popBox {
            display: none;
            background-color: #FFFFFF;
            z-index: 11;
            width: 960px;
            height: 540px;
            position:fixed;
            top:0;
            right:0;
            left:0;
            bottom:0;
            margin:auto;
        }
 
        #popBox .close{
            text-align: right;
            margin-right: 5px;
            background-color: #F8F8F8;
        }
 
        /*关闭按钮*/
        #popBox .close a {
            text-decoration: none;
            color: #2D2C3B;
        }
 
    </style>
<head>
<meta charset="utf-8">
<meta name="renderer" content="webkit|ie-comp|ie-stand">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width,initial-scale=1,minimum-scale=1.0,maximum-scale=1.0,user-scalable=no" />
<meta http-equiv="Cache-Control" content="no-siteapp" />
<link rel="Bookmark" href="/favicon.ico" >
<link rel="Shortcut Icon" href="/favicon.ico" />
<!--[if lt IE 9]>
<script type="text/javascript" src="lib/html5shiv.js"></script>
<script type="text/javascript" src="lib/respond.min.js"></script>
<![endif]-->
<link rel="stylesheet" type="text/css" href="static/h-ui/css/H-ui.min.css" />
<link rel="stylesheet" type="text/css" href="static/h-ui.admin/css/H-ui.admin.css" />
<link rel="stylesheet" type="text/css" href="lib/Hui-iconfont/1.0.8/iconfont.css" />
<link rel="stylesheet" type="text/css" href="static/h-ui.admin/skin/default/skin.css" id="skin" />
<link rel="stylesheet" type="text/css" href="static/h-ui.admin/css/style.css" />
<script type="text/javascript" src="static/jquery-3.4.1.js"></script>
	<script type="text/javascript"
		src="lib/My97DatePicker/4.8/WdatePicker.js"></script>
	<script type="text/javascript"
		src="lib/datatables/1.10.0/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="lib/laypage/1.2/laypage.js"></script>
	<script type="text/javascript"></script>
<!--[if IE 6]>
<script type="text/javascript" src="lib/DD_belatedPNG_0.0.8a-min.js" ></script>
<script>DD_belatedPNG.fix('*');</script>
<![endif]-->
<!--/meta 作为公共模版分离出去-->

<title>收费</title>
<meta name="keywords" content="">
<meta name="description" content="">

		<script type="text/javascript">
		var presDrugs ;
		var jsonArray;
		function search(){
			var registId=document.getElementById("registId").value;
			$("#tbMain  tr:not(:first)").empty("");
			$.ajaxSettings.async = false;
			$.getJSON(
					"GetPatientByRegistIdServlet",
					{"registId":registId},
					function(result){
						var patient = eval(result.patient);
						if(patient != null){
							document.getElementById("patientName").value=patient.name;
							document.getElementById("patientAge").value=patient.age;
							document.getElementById("patientAddress").value=patient.address;
					}
					}
			);
			  var tbody = document.getElementById('tbMain');
			$.getJSON(
					"GetPresDrugByRegistIdServlet",
					{"registId":registId},
					function(result){
						jsonArray = result;
						presDrugs = eval(result);
						$.each(presDrugs,
								function(i,element)
								{
							  		var trow = getDataRow(this); //定义一个方法,返回tr数据
						  	  		trow.setAttribute("class","text-c");
							  		tbody.appendChild(trow);
								});
					}
			);
			$.ajaxSettings.async = true;
		}
		function getDataRow(presDrug){
			var row = document.createElement('tr'); //创建行
			
			var oCell = document.createElement('td');
			var o=document.createElement("input");
			o.name = "checkbox";
			o.type="checkbox";
			o.addEventListener("click", function (){
				var mainCheckbox = document.getElementById('mainCheckbox');
				var items = document.getElementsByName('checkbox');
				var flag=true;
				for(var i=0; i<items.length; i++) {
	                  if(!items[i].checked) {
	                    flag = false;
	                   }
	               }
				mainCheckbox.checked=flag;
			});
			oCell.appendChild(o);
			row.appendChild(oCell);
			
			var idCell = document.createElement('td'); //name
			idCell.innerHTML = presDrug.id; //填充数据
			row.appendChild(idCell); //加入行  ，下面类似
			
			var preIdCell = document.createElement('td'); //name
			preIdCell.innerHTML = presDrug.prescriptionId; //填充数据
			row.appendChild(preIdCell); //加入行  ，下面类似
			
			var nameCell = document.createElement('td'); //name
			nameCell.innerHTML = presDrug.name; //填充数据
			row.appendChild(nameCell); //加入行  ，下面类似
			
			var codeCell = document.createElement('td'); //name
			codeCell.innerHTML = presDrug.code; //填充数据
			row.appendChild(codeCell); //加入行  ，下面类似
			
			var unitCell = document.createElement('td'); //name
			unitCell.innerHTML = presDrug.unit; //填充数据
			row.appendChild(unitCell); //加入行  ，下面类似
			
			var priceCell = document.createElement('td'); //name
			priceCell.innerHTML = presDrug.price; //填充数据
			row.appendChild(priceCell); //加入行  ，下面类似
			
			var numbersCell = document.createElement('td'); //name
			numbersCell.innerHTML = presDrug.numbers; //填充数据
			row.appendChild(numbersCell); //加入行  ，下面类似
			
			return row;
		}
		
		$('.table-sort').dataTable({
			"aaSorting": [[ 1, "desc" ]],//默认第几个排序
			"bStateSave": true,//状态保存
			"pading":false,
			"aoColumnDefs": [
			  //{"bVisible": false, "aTargets": [ 3 ]} //控制列的隐藏显示
			  {"orderable":false,"aTargets":[0,8]}// 不参与排序的列
			]
		});
		
		function selectAll(){
			var mainCheckbox = document.getElementById('mainCheckbox');
			var items = document.getElementsByName('checkbox');
			if(mainCheckbox.checked){
				 for(var i=0; i<items.length; i++) {
                     if(!items[i].checked) {
                    	 items[i].checked = true;
                     }
                 }
			}
			else{
				for(var i=0; i<items.length; i++) {
                    if(items[i].checked) {
                   	 items[i].checked = false;
                    }
                }
			}
				
		}
		
		function popToBox(){
			var popBox = document.getElementById("popBox");
	        var popLayer = document.getElementById("popLayer");
	        
	        var popRegistId = document.getElementById("popRegistId");
	        popRegistId.value = document.getElementById("registId").value;
	        
	        var popName = document.getElementById("popName");
	        popName.value = document.getElementById("patientName").value;
	        
			var items = document.getElementsByName('checkbox');
			var tab = document.getElementById('tbMain');
			var amount = 0;
	        for(var i=0; i<items.length; i++) {
	        	if(items[i].checked)
	        		amount = amount + parseFloat(tab.rows[i+1].cells[6].innerHTML);
	        }
	       var popAmount = document.getElementById("popAmount");
	       popAmount.value = amount;
	       
	        popBox.style.display = "block";
	        popLayer.style.display = "block";
		}
		
		function closeBox() {
	        var popBox = document.getElementById("popBox");
	        var popLayer = document.getElementById("popLayer");
	        popBox.style.display = "none";
	        popLayer.style.display = "none";
	    }
		
		function checkPaidAmount(){
			var paidAmount = document.getElementById("popPaidAmount");
			var amount = document.getElementById("popAmount");
			var changeAmount = document.getElementById("popChangeAmount");
			if(parseFloat(paidAmount.value) < parseFloat(amount.value)){
				document.getElementById("amountMessage").style.display = "block";
			}
			else
				changeAmount.value = parseFloat(paidAmount.value) - parseFloat(amount.value);
			changeAmount.value=Math.round(changeAmount.value*100)/100;
		}
		
		function payment(){
			var items = document.getElementsByName('checkbox');
			var tab = document.getElementById('tbMain');
			var n=0;
			$.each(presDrugs,
					function(i,element)
					{
						if(!items[n].checked)
							this.flag=1;
			        	n++;
					});
			
			$.ajax({
				url:"PaymentServlet",
				type:"post",
				dataType:"json",	
				data:JSON.stringify(presDrugs),
				success:function(result,testStatus){
					alert("缴费成功");
					window.location.reload();
				},
				error :function(){
					
				}
				
			})
		}
		
		</script>
<body>
<article class="page-container">
		<div class="row cl">
			<label class="form-label col-xs-1 col-sm-1"><span class="c-red">*</span>挂号ID：</label>
			
			<div class="formControls col-xs-2 col-sm-2">
				<input type="text" class="input-text" value="" placeholder="" id="registId" name="registId" >
			</div>
			<!-- 先查询该病人原来是否就诊若就诊就将原有病人信息调出来若没有就重新输入。-->
			<div class="formControls col-xs-2 col-sm-2">
				<input class="btn btn-primary radius" type="button"  onclick="search()" value="&nbsp;&nbsp;查询&nbsp;&nbsp;">
			</div>
			
					
			<label class="form-label col-xs-5 col-sm-5"></label>
		</div>
		
		
		<div class="row cl">
			<label class="form-label col-xs-1 col-sm-1">病人信息</label>
			<label class="form-label col-xs-11 col-sm-11"></label>
		</div>
		
		
		
		<div class="row cl">	
			<label class="form-label col-xs-1 col-sm-1"><span class="c-red">*</span>姓名：</label>
			<div class="formControls col-xs-2 col-sm-2">
				<input type="text" class="input-text" placeholder="" id="patientName" name="patientName">
			</div>
			
			
			<label class="form-label col-xs-1 col-sm-1"><span class="c-red">*</span>年龄：</label>
			<div class="formControls col-xs-1 col-sm-1">
				<input type="text" class="input-text" value="" placeholder="" id="patientAge" name="patientAge">
			</div>

			
		<label class="form-label col-xs-1 col-sm-1"><span class="c-red">*</span>家庭地址：</label>
			<div class="formControls col-xs-2 col-sm-2">
				<input type="text" class="input-text" placeholder="" id="patientAddress" name="patientAddress">
			</div>
		</div>
			

		<!--本次挂号信息  -->	
		<div class="row cl">
			<label class="form-label col-xs-1 col-sm-1">挂号信息</label>
			<label class="form-label col-xs-11 col-sm-11"></label>
		</div>

		</article>
<div class="mt-20">
			<table
				class="table table-border table-bordered table-bg table-hover table-sort table-responsive" id="tbMain">
				<thead>
					<tr class="text-c">
						<th width="25"><input type="checkbox" name="mainCheckbox" id="mainCheckbox"  onclick="selectAll()"></th>
						<th width="200" >药品id</th>
						<th width="200">处方id</th>
						<th width="200">药品名称</th>
						<th width="200">编码</th>
						<th width="40">计量单位</th>
						<th width="80">价格</th>
						<th width="40">数量</th>
					</tr>
				</thead>
				</table>
				</div>
<input class="btn btn-primary radius" type="button" name="popBox" value="收费结算" onclick="popToBox()">
<div id="popLayer"></div>
<div id="popBox">
    <div class="close">
        <a href="javascript:void(0)" onclick="closeBox()">×</a>
</div>
     <div style="width:100%" align="center" ><font size=5>交费</div>
            	<label class="form-label col-xs-4 col-sm-2">挂号ID：</label>
				<div class="formControls col-xs-2 col-sm-2">
					<input  type="text" class="input-text" value="" placeholder="" id="popRegistId" name="popRegistId">
				</div>
				<br></br>
				<label class="form-label col-xs-4 col-sm-2">患者姓名：</label>
				<div class="formControls col-xs-2 col-sm-2">
					<input  type="text" class="input-text" value="" placeholder="" id="popName" name="popName">
				</div>
				<br></br>
				<label class="form-label col-xs-4 col-sm-2">应收金额：</label>
				<div class="formControls col-xs-2 col-sm-2">
					<input  type="text" class="input-text" value="" placeholder="" id="popAmount" name="popAmount">
				</div>
				<br></br>
				<label class="form-label col-xs-4 col-sm-2">实收金额：</label>
				<div class="formControls col-xs-2 col-sm-2">
					<input  type="text" class="input-text" value="" placeholder="" id="popPaidAmount" name="popPaidAmount"oninput="checkPaidAmount()">	
				</div>
				
				<br></br>
				<label class="form-label col-xs-4 col-sm-2">找零金额：</label>
				<div class="formControls col-xs-2 col-sm-2">
					<input  type="text" class="input-text" value="" placeholder="" id="popChangeAmount" name="popChangeAmount">
				</div>
				<br></br>

			<div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal" id=""   onclick="closeBox()">取消</button>
                <button type="button" class="btn btn-primary" id="charge"   onclick="payment()">收费</button>
            </div>
</div>
</head>
   
</body>
</html>