package cn.neu.edu.Dao;

import java.sql.SQLException;
import java.util.ArrayList;
import cn.neu.edu.Entity.Drug;

public class PrescriptionDao {
	private static PrescriptionDao instance = new PrescriptionDao();
	
	private PrescriptionDao(){
	}
	
	public static PrescriptionDao getInstance() {
		return instance;
	}
	
	public Drug getDrugById(int id){
		Drug drug = null;
		String sql = "select * from drug where id=?";
		Object[] list = new Object[1];
		list[0] = id;
		DBUtil.excuteQuery(sql, list);
		try {
			if(DBUtil.getResultSet().next()) {
				drug = new Drug(DBUtil.getResultSet().getInt(1), DBUtil.getResultSet().getString(2), DBUtil.getResultSet().getString(3), DBUtil.getResultSet().getString(4), DBUtil.getResultSet().getString(5), DBUtil.getResultSet().getString(6), DBUtil.getResultSet().getInt(7), DBUtil.getResultSet().getInt(8));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.closeAll();
		return drug;
	}
	
	public ArrayList<Drug> getDrugsByName(String name){
		ArrayList<Drug> drugs = new ArrayList<>();
		String sql = "select * from drug where name like ? ";
		name = "%" + name +  "%";
		Object[] list = new Object[1];
		list[0] = name;
		DBUtil.excuteQuery(sql, list);
		try {
			while(DBUtil.getResultSet().next()) {
				drugs.add(new Drug(DBUtil.getResultSet().getInt(1), DBUtil.getResultSet().getString(2), DBUtil.getResultSet().getString(3), DBUtil.getResultSet().getString(4), DBUtil.getResultSet().getString(5), DBUtil.getResultSet().getString(6), DBUtil.getResultSet().getInt(7), DBUtil.getResultSet().getInt(8)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.closeAll();
		return drugs;
	}
	
	public int creatPrescription(int registId, int medicalNum, String name) {
		String sql = "insert into prescription values(null ,?, ?, ?,null)";
		Object[] list = new Object[3];
		list[0] = registId;
		list[1] = medicalNum;
		list[2] = name;
		DBUtil.excuteUpdate(sql, list);
		String sql1 = "select max(id) from prescription";
		DBUtil.excuteQuery(sql1, null);
		int id=-1;
		try {
			if(DBUtil.getResultSet().next())
				id = DBUtil.getResultSet().getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBUtil.closeAll();
		return id;
	}
	
	public int getMedicalNumByRegistId(int registId) {
		String sql = "select medicalnum from prescription where registid=?";
		Object[] list = new Object[1];
		int medicalNum = -1;
		list[0] = registId;
		DBUtil.excuteQuery(sql, list);
		try {
			if(DBUtil.getResultSet().next())
				medicalNum = DBUtil.getResultSet().getInt(1);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBUtil.closeAll();
		return medicalNum;
	}
	
	public void insertPresDrug(int preId, int drugId, String frequency, String usage, int numbers) {
		String sql = "insert into presdrug values(?,?,?,?,?,null)";
		Object[] list = new Object[5];
		list[0] =  preId;
		list[1] =  drugId;
		list[2] =  frequency;
		list[3] =  usage;
		list[4] =  numbers;
		DBUtil.excuteUpdate(sql, list);
		
	}
	
}
