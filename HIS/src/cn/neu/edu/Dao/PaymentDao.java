package cn.neu.edu.Dao;

import java.sql.SQLException;
import java.util.ArrayList;

import cn.neu.edu.Entity.Patient;
import cn.neu.edu.Entity.PresDrug;

public class PaymentDao {
private static PaymentDao instance = new PaymentDao();
	
	private PaymentDao(){
	}
	
	public static PaymentDao getInstance() {
		return instance;
	}
	
	public Patient getPatientByRegistId(int registId) {
		String sql = "select * from patient where idnum in (select patientidnum from register where id = ?)";
		Object[] list = new Object[1];
		list[0] = registId;
		DBUtil.excuteQuery(sql, list);
		Patient p = new Patient();
		try {
			if(DBUtil.getResultSet().next()) {
				p.setName(DBUtil.getResultSet().getString(1));
				p.setSex(DBUtil.getResultSet().getInt(2));
				p.setBirthday(DBUtil.getResultSet().getString(3));
				p.setAge(DBUtil.getResultSet().getInt(4));
				p.setAgetype(DBUtil.getResultSet().getString(5));
				p.setIdNum(DBUtil.getResultSet().getString(6));
				p.setAddress(DBUtil.getResultSet().getString(7));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.closeAll();
		return p;
	}
	
	public ArrayList<PresDrug> getPresDrugByRegistId(int registId) {
		ArrayList<PresDrug> drugs = new ArrayList<PresDrug>();
		String sql = "select drug.id, drug.name, code, unit, price, numbers , preid "
				+ " from prescription join presdrug on prescription.id=preid join drug on drugid= drug.id "
				+ " where registid = ? and presdrug.state=0";
		Object[] list = new Object[1];
		list[0] = registId;
		DBUtil.excuteQuery(sql, list);
		try {
			while(DBUtil.getResultSet().next()) {
				drugs.add(new PresDrug(DBUtil.getResultSet().getInt(1), DBUtil.getResultSet().getString(2), DBUtil.getResultSet().getString(3),
						DBUtil.getResultSet().getString(4), DBUtil.getResultSet().getDouble(5), DBUtil.getResultSet().getInt(6),DBUtil.getResultSet().getInt(7)));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		DBUtil.closeAll();
		return drugs;
	}
	
	public void changePresDrugState(int preId, int drugId ,int state) {
		String sql = "update presdrug set state=? where preid=? and drugid=?";
		Object[] list = new Object[3];
		list[0] = state;
		list[1] = preId;
		list[2] = drugId;
		DBUtil.excuteUpdate(sql, list);
	}
	
	public void insertInvoice(double amount) {
		String sql = "insert into invoice (cost , time) values (?,now())";
		Object[] list = new Object[1];
		list[0] = amount;
		DBUtil.excuteUpdate(sql, list);
	}
	
	public void insertPayment(double price , int numbers,int type, int invid , int presId , int drugId) {
		String sql = "insert into payment values (null ,?,?,?,?,?,?)";
		Object[] list = new Object[6];
		list[0] = price;
		list[1] = numbers;
		list[2] = type;
		list[3] = invid;
		list[4] = presId;
		list[5] = drugId;
		DBUtil.excuteUpdate(sql, list);
	}
	
	public int getMaxInvoiceId() {
		int id = -1;
		String sql = "select max(id) from invoice";
		DBUtil.excuteQuery(sql, null);
		try {
			if(DBUtil.getResultSet().next()) {
				id = DBUtil.getResultSet().getInt(1);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		DBUtil.closeAll();
		return id;
	}
	
}
