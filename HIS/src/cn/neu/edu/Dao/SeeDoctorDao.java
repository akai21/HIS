package cn.neu.edu.Dao;

import java.sql.SQLException;
import java.util.ArrayList;

import cn.neu.edu.Entity.Disease;
import cn.neu.edu.Entity.MedicalHistory;
import cn.neu.edu.Entity.Patient;

public class SeeDoctorDao {
private static SeeDoctorDao instance = new SeeDoctorDao();
	
	private SeeDoctorDao(){
	}
	public static SeeDoctorDao getInstance() {
		return instance;
	}
	
	public String getPasswordByLoginName(String loginName) {
		String sql = "select password from user where loginname = ?";
		Object[] list = new Object[1];
		list[0] = loginName;
		DBUtil.excuteQuery(sql, list);
		try {
			if(DBUtil.getResultSet().next())
				return DBUtil.getResultSet().getString(1);
		} catch (SQLException e) {
		}
		DBUtil.closeAll();
		return null;
	}
	
	public ArrayList<MedicalHistory> getMedicalHistoryByLoginName(int loginName) {
		ArrayList<MedicalHistory> historys = new ArrayList<>();
		String sql = "select * from medicalhistory where registid in (select register.id from register join user on docid=user.id where loginname=?);";
		Object[] list = new Object[1];
		list[0] = loginName;
		DBUtil.excuteQuery(sql, list);
		try {
			while(DBUtil.getResultSet().next()) {
				historys.add(new MedicalHistory(DBUtil.getResultSet().getInt(1), DBUtil.getResultSet().getInt(2), DBUtil.getResultSet().getString(3), DBUtil.getResultSet().getString(4), DBUtil.getResultSet().getString(5), 
						DBUtil.getResultSet().getString(6), DBUtil.getResultSet().getString(7), DBUtil.getResultSet().getString(8),
						DBUtil.getResultSet().getString(9), DBUtil.getResultSet().getInt(10), DBUtil.getResultSet().getString(11)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return historys;
	}
	
	public ArrayList<MedicalHistory> getMedicalHistoryByPatientName(String name) {
		ArrayList<MedicalHistory> historys = new ArrayList<>();
		String sql = "select * from medicalhistory where registid in (select register.id from register join patient on patientidnum=idnum where name=?)";
		Object[] list = new Object[1];
		list[0] = name;
		DBUtil.excuteQuery(sql, list); 
		try {
			while(DBUtil.getResultSet().next()) {
				historys.add(new MedicalHistory(DBUtil.getResultSet().getInt(1), DBUtil.getResultSet().getInt(2), DBUtil.getResultSet().getString(3), DBUtil.getResultSet().getString(4), DBUtil.getResultSet().getString(5), 
						DBUtil.getResultSet().getString(6), DBUtil.getResultSet().getString(7), DBUtil.getResultSet().getString(8),
						DBUtil.getResultSet().getString(9), DBUtil.getResultSet().getInt(10), DBUtil.getResultSet().getString(11)));
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return historys;
	}
	
	  public ArrayList<String> getPatientNameByLoginName(String loginName){
		  ArrayList<String> names = new ArrayList<>();
		  String sql = "select patient.name from patient join register on idnum=patientidnum join user on docid=user.id where loginname=?";
		  Object[] list = new Object[1];
		  list[0] = loginName;
		  DBUtil.excuteQuery(sql, list); 
		  try {
			while(DBUtil.getResultSet().next()) {
				  names.add(DBUtil.getResultSet().getString(1));
			  }
		} catch (SQLException e) {
			e.printStackTrace();
		}
		  return names;
	  }
	  
	  public ArrayList<Patient> getPatientByHealState(int state) {
		  ArrayList<Patient> patients = new ArrayList<>();
			String sql = "SELECT  *  from patient WHERE idnum in (select patientidnum from register where healstate=?)";
			Object[] list = new Object[1];
			list[0] = state;
			DBUtil.excuteQuery(sql, list);
			try {
				while(DBUtil.getResultSet().next()) {
					patients.add(new Patient(
				DBUtil.getResultSet().getString(1),
				DBUtil.getResultSet().getInt(2),
				DBUtil.getResultSet().getString(3),
				DBUtil.getResultSet().getInt(4),
				DBUtil.getResultSet().getString(5),
				DBUtil.getResultSet().getString(6),
				DBUtil.getResultSet().getString(7)));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			DBUtil.closeAll();
			return patients;
		}
	  
	
	  public boolean updateMedicalHistory(String name,String chiefComplaint,String currentMedicalHistory
			  ,String treatmentSituation,
			  String previous,String Allergies
			  ,String examination,String suggestion 
			  ,int state ,String precautions ) {
		  String sql = "update medicalhistory set chiefComplaint=?,currentMedicalHistory=?,treatmentSituation=?,previous=?"
		  		+ ",Allergies=?,examination=?,suggestion=?,state=?,precautions=? "
		  		+ "where registid in (select id from register join patient on patientidnum=idnum where name=?)";
		  Object[] list = new Object[10];
			list[0] = chiefComplaint;
			list[1] = currentMedicalHistory;
			list[2] = treatmentSituation;
			list[3] = previous;
			list[4] = Allergies;
			list[5] = examination;
			list[6] = suggestion;
			list[7] = state;
			list[8] = precautions;
			list[9] = name;
			int i = DBUtil.excuteUpdate(sql, list); 
			if(i==0)
				return false;
			else 
				return true;
	  }
	  
	  public ArrayList<Disease> getAllDisease(){
		  ArrayList<Disease> diseases = new ArrayList<>();
		  String sql = "select name from disease";
		  DBUtil.excuteQuery(sql, null);
			try {
				while(DBUtil.getResultSet().next()) {
					diseases.add(new Disease(DBUtil.getResultSet().getString(1)));
				}
			} catch (SQLException e) {
				e.printStackTrace();
			}
			DBUtil.closeAll();
			return diseases;
	  }
	  
	  public int getRegistidByName(String patientName) {
		  int result = -1;
		  String sql = "select id from register join patient on idnum=patientidnum where name = ?";
		  Object[] list = new Object[1];
		  list[0] = patientName;
		  DBUtil.excuteQuery(sql, list);
		  try {
			if(DBUtil.getResultSet().next()) {
				result =  DBUtil.getResultSet().getInt(1);
			  }
			} catch (SQLException e) {
				e.printStackTrace();
			}
		  return result;
	  }
	  
	  public int seeDoctor(int registId, int type, int kind, String diseaseName) {
			String sql = "{call seedoctor(?,?,?,?,?)}";
			Object[] list = new Object[4];
			list[0] = registId;
			list[1] = type;
			list[2] = kind;
			list[3] = diseaseName;
			return DBUtil.setCallableStatementAndExcute(sql, list);
		}
	  
}
