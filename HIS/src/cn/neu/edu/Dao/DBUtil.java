package cn.neu.edu.Dao;
import org.apache.commons.dbcp.BasicDataSource;
import java.sql.*;
public class DBUtil {
	private static String DriverClassName = "com.mysql.cj.jdbc.Driver";
	private static String URL = "jdbc:mysql://localhost:3306/his?useUnicode=true&characterEncoding=UTF-8&useSSL=false&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";
	private static String user = "root";
	private static String psd = "538108";
	private static int InitialSize = 20;
	private static int MaxActive = 10;
	private static BasicDataSource dbcp = null;
	private static Connection conn = null;
	private static PreparedStatement ps = null;
	private static ResultSet rs = null;
	private static CallableStatement cs = null;
	
	public static ResultSet getResultSet() {
		return rs;
	}
	
	public static void setDataSourceWithDBCP() {
		dbcp = new BasicDataSource();
		dbcp.setDriverClassName(DriverClassName);
		dbcp.setUrl(URL);
		dbcp.setUsername(user);
		dbcp.setPassword(psd);
		dbcp.setInitialSize(InitialSize);
		dbcp.setMaxActive(MaxActive);
	}
	
	public static void getConnection() {
		try {
			conn = dbcp.getConnection();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static void setPreparedStatement(String sql, Object[] list) {//设置PreparedStatement
		 setDataSourceWithDBCP();
		 getConnection();
		try {
			System.out.println(conn==null);
			ps = conn.prepareStatement(sql);
			if(list != null) {
				for(int i = 0;i < list.length;i++)
					ps.setObject(i+1, list[i]);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
	
	}
	
	
	
	public static void excuteQuery(String sql, Object[] list) {
		setPreparedStatement(sql, list);
		 try {
			rs = ps.executeQuery();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public static int excuteUpdate(String sql, Object[] list){
		setPreparedStatement(sql, list);
		int result = -1;
		try {
			result = ps.executeUpdate();
		} catch (SQLException e) {
			e.printStackTrace();
		}finally {
			closeAll();
		}
		return result;
	}
	
	public static void closeAll(){//关闭连接
			try {
				if(rs != null)
				rs.close();
				if(ps!=null)
					ps.close();
				if(conn!=null)
					conn.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
	}
	
	public static int setCallableStatementAndExcute(String sql, Object[] list) {//设置并执行CallableStatement
		int result = -1;
		 setDataSourceWithDBCP();
		 getConnection();
		try {
			cs = conn.prepareCall(sql);
			if(list != null) {
				for(int i = 0;i < list.length;i++) {
					cs.setObject(i+1, list[i]);
				}
			}
			cs.registerOutParameter(list.length+1, Types.INTEGER);
			cs.execute();
			result = cs.getInt(list.length+1); 
			closeAll();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return result;
	}
	
}
