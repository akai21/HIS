package cn.neu.edu.Service;

import java.util.ArrayList;

import cn.neu.edu.Dao.SeeDoctorDao;
import cn.neu.edu.Entity.Disease;
import cn.neu.edu.Entity.Patient;

public class SeeDoctorService {
	private static SeeDoctorService instance = new SeeDoctorService();
	private SeeDoctorDao dao = SeeDoctorDao.getInstance();
	private SeeDoctorService() {}
	public static SeeDoctorService getInstance() {
		return instance;
	}
	public boolean checkPassword(String loginName, String password) {
		String realPsd = dao.getPasswordByLoginName(loginName);
		if(realPsd == null)
			return false;
		if(realPsd.equals(password))
			return true;
		return false;
	}
	
	public ArrayList<Patient> getHealedPatient() {
		return  dao.getPatientByHealState(2);
	}
	public ArrayList<Patient> getNoHealedPatient() {
		return  dao.getPatientByHealState(1);
	}
	  public boolean updateMedicalHistory(String name,String chiefComplaint,String currentMedicalHistory
			  ,String treatmentSituation,
			  String previous,String Allergies
			  ,String examination,String suggestion 
			   ,String precautions ) {
		  int state = 3;//更改状态为已诊毕
		  return dao.updateMedicalHistory(name, chiefComplaint, currentMedicalHistory, treatmentSituation, 
				  previous, Allergies, examination, suggestion, state, precautions);
	  }
	  public int seeDoctor(String patientName, int type, int kind, String diseaseName) {
		  int registId = dao.getRegistidByName(patientName);
		  if(registId == -1)
			  return 2;//患者不存在
		  return dao.seeDoctor(registId, type, kind, diseaseName);
	  }

	  public ArrayList<Disease> getAllDisease(){
		  return dao.getAllDisease();
	  }
	  
//	public static void main(String[] args) {
//		SeeDoctorService s = SeeDoctorService.getInstance();
//	}
}
