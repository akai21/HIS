package cn.neu.edu.Service;

import java.util.ArrayList;

import cn.neu.edu.Dao.RegisterDao;
import cn.neu.edu.Entity.Department;
import cn.neu.edu.Entity.Patient;
import cn.neu.edu.Entity.Register;
import cn.neu.edu.Entity.User;

public class RegistService {
	private static RegistService instance = new RegistService();
	private RegisterDao dao = RegisterDao.getInstance();
	private RegistService() {}
	public static RegistService getInstance() {
		return instance;
	}
	public ArrayList<Register> getRegisterByMedicalNum(int medicalNumber, int pageNumber, int pageSize){
		return dao.getRegisterByMedicalNum(medicalNumber, pageNumber*pageSize, pageSize);
	}
	public int dropRegist(int id) {
		int state = dao.getRegisterState(id);
		if(state == 1)
			dao.dropRegist(id);
		return state;
	}
	public Patient getPatientByIdNum(String idNum) {
		return dao.getPatientByIdNum(idNum);
	}
	public int regist(String name ,int sex , String birthday ,int age ,String agetype ,String idNnum,
			String address ,int docid ,int whetherMedical ,String noon  ) {
		return dao.regist(name, sex, birthday, age, agetype, idNnum, address, docid, 1, whetherMedical, 1, noon);
	}
	
	public ArrayList<Department> getAllDepartment() {
		return dao.getAllDepartment();
	}
	public ArrayList<User> getDoctorByDepidLevelid(int depid, int levelId){
		return dao.getDoctorByDepidLevelid(depid, levelId);
	}
	
	public double getRegistCostById(int id) {
		return dao.getRegistCostById(id);
	}
	
	public static  void main(String[] args) {
		RegistService r = RegistService.getInstance();
		for(User u :r.getDoctorByDepidLevelid(1, 1) ){
			System.out.println(u.getName());
		}
	}
}
