package cn.neu.edu.Service;

import java.util.ArrayList;

import cn.neu.edu.Dao.PaymentDao;
import cn.neu.edu.Entity.Patient;
import cn.neu.edu.Entity.PresDrug;

public class PaymentService {
	private PaymentDao dao = PaymentDao.getInstance();
	private static PaymentService instance = new PaymentService();
	
	private PaymentService(){
	}
	
	public static PaymentService getInstance() {
		return instance;
	}
	
	public Patient getPatientByRegistId(int registId) {
		return dao.getPatientByRegistId(registId);
	}
	
	public ArrayList<PresDrug> getPresDrugByRegistId(int registId) {
		return dao.getPresDrugByRegistId(registId);
	}
	
	public void payment(double price , int numbers, int presId , int drugId) {
		int type = 2;//ҩƷ
		int invid = dao.getMaxInvoiceId();
		int state = 2;//δ��ҩ
		dao.changePresDrugState(presId, drugId, state);
		dao.insertPayment(price, numbers, type, invid, presId, drugId);
	
	}
	
	public void insertInvoice(double amount) {
		dao.insertInvoice(amount);
	}
	
}
