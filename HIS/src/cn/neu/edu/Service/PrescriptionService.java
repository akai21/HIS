package cn.neu.edu.Service;

import java.util.ArrayList;

import cn.neu.edu.Dao.PrescriptionDao;
import cn.neu.edu.Entity.Drug;

public class PrescriptionService {
	private static PrescriptionService instance = new PrescriptionService();
	private PrescriptionDao dao = PrescriptionDao.getInstance();
	private PrescriptionService() {}
	public static PrescriptionService getInstance() {
		return instance;
	}
	
	public ArrayList<Drug> getDrugsByName(String name){
		return dao.getDrugsByName(name);
	}
	
	public Drug getDrugById(int id){
		return dao.getDrugById(id);
	}
	
	public int creatPrescription(int registId, String name) {
		int medicalNum = dao.getMedicalNumByRegistId(registId);
		return dao.creatPrescription(registId, medicalNum, name);
	}

	public void insertPresDrug(int preId, int drugId, String frequency, String usage, int numbers) {
		dao.insertPresDrug(preId, drugId, frequency, usage, numbers);
	}

	
	public static void main(String[] args) {
		ArrayList<Drug> a = PrescriptionService.getInstance().getDrugsByName("����");
		System.out.println(a.size());
	}
	
}
