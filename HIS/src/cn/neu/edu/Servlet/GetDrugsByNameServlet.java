package cn.neu.edu.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.neu.edu.Entity.Drug;
import cn.neu.edu.Service.PrescriptionService;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class GetDrugsByNameServlet
 */
@WebServlet("/GetDrugsByNameServlet")
public class GetDrugsByNameServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetDrugsByNameServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject  json = new JSONObject();
		String name  = request.getParameter("drugName");
		ArrayList<Drug> drugs = PrescriptionService.getInstance().getDrugsByName(name);
	
		try {
			int id = Integer.parseInt(name);
			drugs.add(PrescriptionService.getInstance().getDrugById(id));
		}catch(Exception e) {
			
		}
		
		for(int i=0;i < drugs.size();i++)
			json.put("drug"+(i+1), drugs.get(i));
		out.print(json);

	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
