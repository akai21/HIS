package cn.neu.edu.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.neu.edu.Entity.User;
import cn.neu.edu.Service.RegistService;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class GetDoctorServlet
 */
@WebServlet("/GetDoctorServlet")
public class GetDoctorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetDoctorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		int registlevel = Integer.parseInt(request.getParameter("registlevel"));
		int depId = Integer.parseInt(request.getParameter("department"));
		ArrayList<User> docs = RegistService.getInstance().getDoctorByDepidLevelid(depId, registlevel);
		PrintWriter out = response.getWriter();
		JSONObject  json = new JSONObject();
		for(int i=0;i < docs.size();i++)
			json.put("doc"+(i+1), docs.get(i));
		out.print(json);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
