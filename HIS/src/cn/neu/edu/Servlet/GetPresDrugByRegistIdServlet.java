package cn.neu.edu.Servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import cn.neu.edu.Entity.PresDrug;
import cn.neu.edu.Service.PaymentService;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class GetPresDrugByRegistIdServlet
 */
@WebServlet("/GetPresDrugByRegistIdServlet")
public class GetPresDrugByRegistIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetPresDrugByRegistIdServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		JSONObject  json = new JSONObject();
		int registId  = Integer.parseInt(request.getParameter("registId"));
		request.getSession().setAttribute("registId", registId);
		ArrayList<PresDrug> presDrugs = PaymentService.getInstance().getPresDrugByRegistId(registId);
		for(int i=0;i < presDrugs.size();i++)
			json.put("presDrug"+(i+1), presDrugs.get(i));
		out.print(json);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
