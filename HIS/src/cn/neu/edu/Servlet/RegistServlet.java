package cn.neu.edu.Servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.neu.edu.Service.RegistService;

/**
 * Servlet implementation class RegistServlet
 */
@WebServlet("/RegistServlet")
public class RegistServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public RegistServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String name = request.getParameter("patientName");
		int sex  = Integer.parseInt(request.getParameter("patientSex"));
		String birthday  = request.getParameter("patientBirthday");
		int age  = Integer.parseInt(request.getParameter("patientAge"));
		String agetype  = request.getParameter("patientAgeType");
		String idNnum = request.getParameter("patientNum");
		String address = request.getParameter("patientAddress");
		int docid = Integer.parseInt(request.getParameter("doctor"));
		int whetherMedical = Integer.parseInt(request.getParameter("whetherMedical"));
		String noon = request.getParameter("noon");
		
		int result = RegistService.getInstance().regist(name, sex, birthday, age, agetype, idNnum, address, docid, whetherMedical, noon);
		System.out.println(result);
		if(result == 0)
			request.setAttribute("result", "�ɹ�");
		else
			request.setAttribute("result", "ʧ��");
		request.getRequestDispatcher("end.jsp");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
