package cn.neu.edu.Servlet;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.neu.edu.Entity.PresDrug;
import cn.neu.edu.Service.PaymentService;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class PaymentServlet
 */
@WebServlet("/PaymentServlet")
public class PaymentServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PaymentServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html;charset=utf-8");
		 
		/** 设置响应头允许ajax跨域访问 **/
		response.setHeader("Access-Control-Allow-Origin", "*");
		/* 星号表示所有的异域请求都可以接受， */
		response.setHeader("Access-Control-Allow-Methods", "GET,POST");
	
		//将json对象转换为java对象
		BufferedReader br = new BufferedReader(new InputStreamReader(request.getInputStream(),"utf-8"));
		String line = null;
		StringBuilder sb = new StringBuilder();
		while ((line = br.readLine()) != null) {
			sb.append(line);
		}
		//将json字符串转换为json对象
		JSONObject json=JSONObject.fromObject(sb.toString());
		//将json对象转换为java对象
		Iterator iterator1 = json.keys();
		Iterator iterator2 = json.keys();
		double amount = 0;
		PaymentService ps = PaymentService.getInstance();
		
		while(iterator1.hasNext()){
			String key = (String) iterator1.next();
			PresDrug presDrug = (PresDrug)JSONObject.toBean(json.getJSONObject(key),PresDrug.class);
			if(presDrug.getFlag() == 0) {
		    amount += presDrug.getPrice() * presDrug.getNumbers();
			}
		}
		
		ps.insertInvoice(amount);
		
		while(iterator2.hasNext()){
			String key = (String) iterator2.next();
			PresDrug presDrug = (PresDrug)JSONObject.toBean(json.getJSONObject(key),PresDrug.class);
			if(presDrug.getFlag() == 0) 
		    ps.payment(presDrug.getPrice(), presDrug.getNumbers(), presDrug.getPrescriptionId(), presDrug.getId());
		}
		
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
