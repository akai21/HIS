package cn.neu.edu.Servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.neu.edu.Entity.Patient;
import cn.neu.edu.Service.PaymentService;
import net.sf.json.JSONObject;

/**
 * Servlet implementation class GetPatientByRegistIdServlet
 */
@WebServlet("/GetPatientByRegistIdServlet")
public class GetPatientByRegistIdServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetPatientByRegistIdServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		response.setContentType("text/html; charset=UTF-8");
		PrintWriter out = response.getWriter();
		int registId = Integer.parseInt(request.getParameter("registId"));
		PaymentService r = PaymentService.getInstance();
		Patient p = r.getPatientByRegistId(registId);
		JSONObject  json = new JSONObject();
		json.put("patient", p);
		out.print(json);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
