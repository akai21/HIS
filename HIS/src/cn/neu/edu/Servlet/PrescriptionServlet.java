package cn.neu.edu.Servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.neu.edu.Service.PrescriptionService;

/**
 * Servlet implementation class PrescriptionServlet
 */
@WebServlet("/PrescriptionServlet")
public class PrescriptionServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public PrescriptionServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		int registId = Integer.parseInt(request.getParameter("registId"));
		String prescriptionName = request.getParameter("prescriptionName");
		int counts = Integer.parseInt(request.getParameter("counts"));
		int preId = PrescriptionService.getInstance().creatPrescription(registId, prescriptionName);
		for(int i = 1;i <= counts;i++) {
			int drugId = Integer.parseInt(request.getParameter("drugId" + i));
			System.out.println(drugId);
			String frequency = request.getParameter("frequency" + i);
			String usage = request.getParameter("usage" + i);
			int numbers = Integer.parseInt(request.getParameter("numbers" + i));
			PrescriptionService.getInstance().insertPresDrug(preId, drugId, frequency, usage, numbers);
		}
		PrintWriter out = response.getWriter();
		out.write("true");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
