package cn.neu.edu.Servlet;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import cn.neu.edu.Service.SeeDoctorService;

/**
 * Servlet implementation class SeeDoctorServlet
 */
@WebServlet("/SeeDoctorServlet")
public class SeeDoctorServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public SeeDoctorServlet() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		request.setCharacterEncoding("utf-8");
		String patientName = (String) request.getSession().getAttribute("patientName");
		String chiefComplaint = request.getParameter("chiefcomplaint");
		String currentMedicalHistory = request.getParameter("currentMedicalHistory");
		String treatmentSituation = request.getParameter("treatmentSituation");
		String previous = request.getParameter("previous");
		String Allergies = request.getParameter("Allergies");
		String examination = request.getParameter("examination");
		String suggestion  = request.getParameter("suggestion");
		String precautions = request.getParameter("precautions");
		String disease = request.getParameter("disease");
		int kind = Integer.parseInt(request.getParameter("kind"));
		int type = Integer.parseInt(request.getParameter("type"));
		boolean updateResult = SeeDoctorService.getInstance().updateMedicalHistory(patientName, chiefComplaint, currentMedicalHistory, treatmentSituation, previous, Allergies, examination, suggestion, precautions);
		int seeDoctorResult = SeeDoctorService.getInstance().seeDoctor(patientName, type, kind, disease);
		if(updateResult && seeDoctorResult==0)
			response.sendRedirect("index.html");
		//else
			//response.sendRedirect("Login.html");
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
