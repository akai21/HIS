package cn.neu.edu.Entity;

public class Register {
	private int id;
	private String patientidnum;
	private int healstate;
	private String registdate;
	private String healdate;
	private int docid;
	private int regid;
	private int whetherMedical ;
	private int settlement;
	private String noon;
	public Register(int id, String patientidnum, int healstate, String registdate, String healdate, int docid,
			int regid, int whetherMedical, int settlement, String noon) {
		this.id = id;
		this.patientidnum = patientidnum;
		this.healstate = healstate;
		this.registdate = registdate;
		this.healdate = healdate;
		this.docid = docid;
		this.regid = regid;
		this.whetherMedical = whetherMedical;
		this.settlement = settlement;
		this.noon = noon;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPatientidnum() {
		return patientidnum;
	}
	public void setPatientidnum(String patientidnum) {
		this.patientidnum = patientidnum;
	}
	public int getHealstate() {
		return healstate;
	}
	public void setHealstate(int healstate) {
		this.healstate = healstate;
	}
	public String getRegistdate() {
		return registdate;
	}
	public void setRegistdate(String registdate) {
		this.registdate = registdate;
	}
	public String getHealdate() {
		return healdate;
	}
	public void setHealdate(String healdate) {
		this.healdate = healdate;
	}
	public int getDocid() {
		return docid;
	}
	public void setDocid(int docid) {
		this.docid = docid;
	}
	public int getRegid() {
		return regid;
	}
	public void setRegid(int regid) {
		this.regid = regid;
	}
	public int getWhetherMedical() {
		return whetherMedical;
	}
	public void setWhetherMedical(int whetherMedical) {
		this.whetherMedical = whetherMedical;
	}
	public int getSettlement() {
		return settlement;
	}
	public void setSettlement(int settlement) {
		this.settlement = settlement;
	}
	public String getNoon() {
		return noon;
	}
	public void setNoon(String noon) {
		this.noon = noon;
	}	
}
