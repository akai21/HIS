package cn.neu.edu.Entity;

public class Patient {
	private String name;
	private int sex ;
	private String  birthday ;
	private int age;
	private String agetype ;
	private String idNum;
	private String address;
	public Patient(String name, int sex, String birthday, int age, String agetype, String idNum, String address) {
		this.name = name;
		this.sex = sex;
		this.birthday = birthday;
		this.age = age;
		this.agetype = agetype;
		this.idNum = idNum;
		this.address = address;
	}
	public Patient() {
		super();
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSex() {
		return sex;
	}
	public void setSex(int sex) {
		this.sex = sex;
	}
	public String getBirthday() {
		return birthday;
	}
	public void setBirthday(String birthday) {
		this.birthday = birthday;
	}
	public int getAge() {
		return age;
	}
	public void setAge(int age) {
		this.age = age;
	}
	public String getAgetype() {
		return agetype;
	}
	public void setAgetype(String agetype) {
		this.agetype = agetype;
	}
	public String getIdNum() {
		return idNum;
	}
	public void setIdNum(String idNum) {
		this.idNum = idNum;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
}
