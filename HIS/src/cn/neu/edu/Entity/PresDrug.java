package cn.neu.edu.Entity;

public class PresDrug {
	private int id;
	private String name;
	private String code;
	private String unit;
	private double price;
	private int numbers;
	private int prescriptionId;
	private int flag = 0;//0-δɾ��1-��ɾ��
	
	public PresDrug() {}
	
	public PresDrug(int id, String name, String code, String unit, double price, int numbers, int prescriptionId) {
		this.id = id;
		this.name = name;
		this.code = code;
		this.unit = unit;
		this.price = price;
		this.numbers = numbers;
		this.prescriptionId = prescriptionId;
	}
	
	
	
	public int getFlag() {
		return flag;
	}

	public void setFlag(int flag) {
		this.flag = flag;
	}

	public int getPrescriptionId() {
		return prescriptionId;
	}

	public void setPrescriptionId(int prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCode() {
		return code;
	}
	public void setCode(String code) {
		this.code = code;
	}
	public String getUnit() {
		return unit;
	}
	public void setUnit(String unit) {
		this.unit = unit;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getNumbers() {
		return numbers;
	}
	public void setNumbers(int numbers) {
		this.numbers = numbers;
	}
	
	
}
