package cn.neu.edu.Entity;

public class Drug {
	int id;
	String code;
	String name ;
	String unit ;
	String format;
	String price;
	int type ;
	int dosage;
	
	public Drug(int id, String code, String name, String unit, String format, String price, int type, int dosage) {
		this.id = id;
		this.code = code;
		this.name = name;
		this.unit = unit;
		this.format = format;
		this.price = price;
		this.type = type;
		this.dosage = dosage;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getFormat() {
		return format;
	}

	public void setFormat(String format) {
		this.format = format;
	}

	public String getPrice() {
		return price;
	}

	public void setPrice(String price) {
		this.price = price;
	}

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}

	public int getDosage() {
		return dosage;
	}

	public void setDosage(int dosage) {
		this.dosage = dosage;
	}
	
}
