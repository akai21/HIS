package cn.neu.edu.Entity;

public class MedicalHistory {
	private int medicalNum;
	private int registid;
	private String chiefComplaint;
	private String currentMedicalHistory; 
	private String treatmentSituation;
	private String previous;
	private String Allergies;
	private String examination;
	private String suggestion ;
	private int state ;
	private String precautions ;
	public MedicalHistory(int medicalNum, int registid, String chiefComplaint, String currentMedicalHistory,
			String treatmentSituation, String previous, String allergies, String examination, String suggestion,
			int state, String precautions) {
		this.medicalNum = medicalNum;
		this.registid = registid;
		this.chiefComplaint = chiefComplaint;
		this.currentMedicalHistory = currentMedicalHistory;
		this.treatmentSituation = treatmentSituation;
		this.previous = previous;
		Allergies = allergies;
		this.examination = examination;
		this.suggestion = suggestion;
		this.state = state;
		this.precautions = precautions;
	}
	public int getMedicalNum() {
		return medicalNum;
	}
	public void setMedicalNum(int medicalNum) {
		this.medicalNum = medicalNum;
	}
	public int getRegistid() {
		return registid;
	}
	public void setRegistid(int registid) {
		this.registid = registid;
	}
	public String getChiefComplaint() {
		return chiefComplaint;
	}
	public void setChiefComplaint(String chiefComplaint) {
		this.chiefComplaint = chiefComplaint;
	}
	public String getCurrentMedicalHistory() {
		return currentMedicalHistory;
	}
	public void setCurrentMedicalHistory(String currentMedicalHistory) {
		this.currentMedicalHistory = currentMedicalHistory;
	}
	public String getTreatmentSituation() {
		return treatmentSituation;
	}
	public void setTreatmentSituation(String treatmentSituation) {
		this.treatmentSituation = treatmentSituation;
	}
	public String getPrevious() {
		return previous;
	}
	public void setPrevious(String previous) {
		this.previous = previous;
	}
	public String getAllergies() {
		return Allergies;
	}
	public void setAllergies(String allergies) {
		Allergies = allergies;
	}
	public String getExamination() {
		return examination;
	}
	public void setExamination(String examination) {
		this.examination = examination;
	}
	public String getSuggestion() {
		return suggestion;
	}
	public void setSuggestion(String suggestion) {
		this.suggestion = suggestion;
	}
	public int getState() {
		return state;
	}
	public void setState(int state) {
		this.state = state;
	}
	public String getPrecautions() {
		return precautions;
	}
	public void setPrecautions(String precautions) {
		this.precautions = precautions;
	}
	
	
}
